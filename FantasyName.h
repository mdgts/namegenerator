#ifndef FANTASYNAME_H
#define FANTASYNAME_H

#include <fstream>
#include <string>

class FantasyName
{
    public:
        FantasyName();
        ~FantasyName();
        std::string getName() const;
        void remakeName();
        void setMaxLength(int x);
    private:
        bool determineLast(std::string &someName, std::string &which, int index);
        int max_length;
        std::string intToVowel (int x);
        std::string intToConsonant (int y);
        std::string chooseNextCharacter (std::string &someName);
        std::string consonants;
        std::string vowels;
        std::string name;
        std::ofstream alphabetWriter;
        std::ifstream alphabetReader;
};

#endif // FANTASYNAME_H
