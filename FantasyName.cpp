#include "FantasyName.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <fstream>


using namespace std;

FantasyName::FantasyName()
{
    //alphabetWriter.open("alphabet.txt");
    vowels = "aeiouy";
    consonants = "bcdfghjklmnpqrstvwxyz";
    name = "";
    max_length = 10; // default max length
    remakeName();
}

FantasyName::~FantasyName() {
    //alphabetWriter.close();
    //alphabetReader.close();
}

string FantasyName::getName() const {
    return name;
}

void FantasyName::setMaxLength(int x) {
    if (x > 0)
        max_length = x;
    else {max_length = 10; cout << "\nAn invalid maximum length was entered.  10 was set by default." << endl;}
}

void FantasyName::remakeName() {
    srand(time(0));
    name = "";
    for (int i = 0; i < (1 + rand() % max_length); i++) {
            name.append(chooseNextCharacter(name));
        }
    name[0] = toupper(name[0]);
}

string FantasyName::chooseNextCharacter (string &someName) {
    //check for double vowels, consonants.  If there is a double vowel/consonant, guaranteed the opposite
    if (determineLast(someName,consonants,name.length() - 1) && determineLast(someName,consonants,name.length() - 2))
        return intToVowel(1 + rand() % vowels.length());
    else if (determineLast(someName,vowels,name.length() - 1) && determineLast(someName,vowels,name.length() - 2))
        return intToConsonant(1 + rand() % consonants.length());
    //otherwise, choose randomly between a vowel and a consonant
    else if (1 + rand() % 2 == 2)
        return intToConsonant(1 + rand() % consonants.length());
    else return intToVowel(1 + rand() % vowels.length());
}

string FantasyName::intToConsonant (int y) {
    return consonants.substr(y,1);
}

string FantasyName::intToVowel (int x) {
    return vowels.substr(x,1);
}


bool FantasyName::determineLast(string& someName, string& which, int index) {
    for (unsigned int i = 0; i < which.length(); i++) {
        if (which[i] == name[index])
            return true;
    }

    return false;
}
